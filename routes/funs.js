var cassandra = require('cassandra-driver');
var client = new cassandra.Client( { contactPoints : [ '127.0.0.1' ] } );
client.connect(function(err) {
    console.log("data base connecting",err);
});


function randomcode(){
        var charSet="qwertyuioplkjhgfdsazxcvbnm1234567890";
        var code="";
        for(var x=0;x<5;x=x+1)
                code+=charSet[Math.floor(Math.random() * charSet.length)];
        return code;
};

function genrandom(id,url,res){
        var query='SELECT * FROM urls.short WHERE code=? ALLOW FILTERING;';
        var code=randomcode();
         client.execute(query,[code], function(err, result) {
                if(!result.rows.length>0){
                        inserturl(id,code,url,res);
                }
                else{
                        return genrandom(id,url,res);
                };
        });
}
//Insert to url
function inserturl(id,code,url,res){
         var query='SELECT url,code FROM urls.short  ALLOW FILTERING;';
        var upsertUrl = 'INSERT INTO urls.short (id, code,url) '+ 'VALUES(?, ?, ?);';
        client.execute(upsertUrl,[id, code, url],function(error,result){
                                        if(error){
                                                return false;
                                        }
                                        client.execute(query, function(err, result) {
                                        res.render('index.html', { title: 'UrlShortner',result:result.rows,url:code });
        });
                                });
};

module.exports.randomcode=randomcode;
module.exports.genrandom=genrandom;
module.exports.inserturl=inserturl;
