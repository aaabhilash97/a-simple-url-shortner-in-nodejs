var express = require('express');
var cassandra = require('cassandra-driver');
var router = express.Router();
var client = new cassandra.Client( { contactPoints : [ '127.0.0.1' ] } );
client.connect(function(err) {
    console.log("data base connecting",err);
});
 var qa='SELECT url,code FROM urls.short  ALLOW FILTERING;';
  client.execute(qa, function(err, result) {
		console.log(err,result);
});
var randomcode=require('./funs').randomcode;
var genrandom=require('./funs').genrandom;
var insertUrl=require('./funs').insertUrl;
var render=require('./funs').render;
var fetchall=require('./funs').fetchall;

/* GET home page. */
router.get('/', function(req, res, next) {
    var query='SELECT url,code FROM urls.short  ALLOW FILTERING;';
    fetchall(query,res)
});

//index post
router.post('/', function(req, res,next) {
    var id = null,
		x=req.body.url,
			url=null;
    var getUrlById = 'SELECT * FROM urls.short WHERE url=? ALLOW FILTERING;';
    client.execute(getUrlById,[x], function(err, result) {
	if(result.rows.length>0){
		url=result.rows[0].code;
		var query='SELECT url,code FROM urls.short  ALLOW FILTERING;';
  client.execute(query, function(err, result) {
        res.render('index.html', { title: 'UrlShortner',result:result.rows,url:url });
        });
	}
	else{
			var code=false;
			if ( ! req.body.hasOwnProperty('id')) {
                                        id = cassandra.types.uuid();
                                } else {
                                        id = req.body.id;
                                };
				code=genrandom(id,req.body.url,res);
	};
				
     });
});
router.get('/:id', function(req, res) {
    var getUrlById = 'SELECT * FROM urls.short WHERE code=? ALLOW FILTERING;';
    var code=req.params.id;
    client.execute(getUrlById,[code], function(err, result) {
        if (!result.rows.length>0) {
            res.status(404).send({ msg : 'url not found.' });
        } else {
	   res.redirect(result.rows[0].url);
	};
    });
});
router.post('/shorten',function(req,res){
	console.log(req.body.url);
	var id = null,
                x=req.body.url,
                        url=null;
    var getUrlById = 'SELECT * FROM urls.short WHERE url=? ALLOW FILTERING;';
    client.execute(getUrlById,[x], function(err, result) {
        if(result.rows.length>0){
                url=result.rows[0].code;
        	res.json({url:url });
        }
});
});
module.exports = router;

