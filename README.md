use npm to install project's dependencies

------------------------------------------

$ npm install




Run create_tables.js to create tables for this project

-------------------------------------------------------


$ nodejs create__tables.js




Install Cassandra using APT repositories on Debian and Ubuntu Linux.

----------------------------------------------------------------------

https://www.datastax.com/documentation/cassandra/2.0/cassandra/install/installDeb_t.html




Run Application By Running Command

-------------------

$ ./bin/www
