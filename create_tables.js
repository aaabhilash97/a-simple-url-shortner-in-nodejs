var cassandra = require('cassandra-driver');
var async=require("async");
var client = new cassandra.Client( { contactPoints : [ '127.0.0.1' ] ,keyspace: 'urls'} );

client.connect(function(err) {
    console.log(err);
});

function keyspace(){
	client.execute("CREATE KEYSPACE IF NOT EXISTS urls WITH replication " + "= {'class' : 'SimpleStrategy', 'replication_factor' : 3};",function(err,res){
								console.log(err,res);
							 	tables();	
							});
}


function tables(){
    async.parallel([
        function(next) {
            client.execute('CREATE TABLE IF NOT EXISTS urls.short (' +
                'id uuid,' +
                'code text,' +
                'url text,' +
                'PRIMARY KEY(id,code,url)'+
                ');',
                next);
        }
    ],function(err,res){
								index();
								console.log(err,res);});
};



function index() {
    var index="CREATE INDEX ON urls.short (code);";
    client.execute(index, function(err, result) {
                console.log(err,result);
    });


    var index1="CREATE INDEX ON urls.short (url);"
    client.execute(index1, function(err, result) {
                console.log(err,result);
    });
};
keyspace();
